package com.janoz.vertx.dumpster;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by gijs on 11/05/16.
 */
public class DumpsterVerticleTest {
    @Test
    public void sanatiseFilenameSuccess() throws Exception {
        Arrays.asList(
                "brother bear.mkv",
                "Cobain.Montage.of.Heck.2015.7200p.WEB-DL.1GB.MkvCage.mkv",
                "De Eetclub 2010 1080p.mkv",
                "h-huisvrouw.mkv",
                "Edge.of.Tomorrow.2014.1080p.WEB-DL.DD5.1.H264-RARBG.mkv",
                "Flight.2012.BluRay.1080p.5.1CH.x264.Ganool.mkv",
                "Formula.51.2001.(1080p)HDTV.mkv",
                "Horrible Bosses 2011 1080p x264.mkv",
                "Hugo 2011 720p BRRip AC3 x264 MacGuffin.mkv",
                "Interstellar.2014.720p.BluRay.x264.DTS-RARBG.mkv",
                "Into The Storm 2014 1080p BRRip x264 DTS-JYK.mkv",
                "John.Wick.2014.720p.BRRip.x264.AC3-RARBG.mkv",
                "Kingsman The Secret Service 2014 1080p BRRip x264 DTS-JYK.mkv",
                "Mad Max Fury Road 2015 1080p WEB-DL x264 AC3-JYK.mkv",
                "Paranoia 2013 BluRay 720p DTS x264-MgB [ETRG].mkv",
                "Seeking a Friend for the End of the World (2012) 1080p.mkv",
                "Side Effects 2013 1080p BRRip x264 AC3-JYK.mkv",
                "rhd-super-1080p.mkv",
                "The Big Lebowski 1998 BDRip 720p DTS multisub HighCode.mkv",
                "The.Curious.Case.of.Benjamin.Button.2008.576p.BluRay.x264-VeeD.mkv",
                "The Dark Knight Rises 2012 720p IMAX BluRay DTS x264-MgB.mkv",
                "The.Hangover.Part.II.2011.1080p-LTT.mkv",
                "Intouchables.2011.FRENCH.720p.BluRay.x264.mkv",
                "The.Man.from.U.N.C.L.E.2015.1080p.WEB-DL.x264.AC3-JYK.mkv",
                "The.Maze.Runner.2014.1080p.BRRip.x264.AC3-RARBG.mkv",
                "the.social.network.2010.720p.bluray.dts.x264-hdxt.mkv",
                "The.Spiderwick.Chronicles.2008.x264.AC3-WAF.mkv",
                "cbgb-thetown720.mkv",
                "Total Recall 1990 720p BluRay DTS x264-SK.mkv",
                "Unthinkable.mkv",
                "Upside Down (2012).mkv"
        )
                .stream()
                .forEach(s ->
                        assertEquals(s + " should not change", s, DumpsterVerticle.sanatiseFilename(s)));
    }
    @Test
    public void sanatiseFilenameFail() throws Exception {
        Arrays.asList(
                "\b\b\b/etc/passwd"
        )
                .stream()
                .forEach(s ->
                        assertNotEquals(s + " should change", s, DumpsterVerticle.sanatiseFilename(s)));
    }

}