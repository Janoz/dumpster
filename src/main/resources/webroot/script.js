var previousTime;
var previousSize;

function uploadFile() {
    var file = document.getElementById('fileToUpload').files[0];
    var fd = new FormData();

    previousTime = (new Date()).getTime();
    previousSize = 0;

    fd.append("fileToUpload", file);
    var xhr = new XMLHttpRequest();
    xhr.upload.addEventListener("progress", uploadProgress, false);
    xhr.addEventListener("load", uploadComplete, false);
    xhr.addEventListener("error", uploadFailed, false);
    xhr.addEventListener("abort", uploadCanceled, false);
    xhr.open("PUT", "");
    xhr.setRequestHeader("X_FILENAME", file.name);
    xhr.send(fd);
}

function uploadProgress(e) {
    var progressBar = document.getElementById('progressBar');
    var speedContainer = document.getElementById('speed');
    if (e.lengthComputable) {
        progressBar.max = e.total;
        progressBar.value = e.loaded;

        var currentTime = (new Date()).getTime();
        var elapsed = currentTime - previousTime;
        if (elapsed > 200) {
            var processed = e.loaded - previousSize;
            speedContainer.innerHTML = toSpeedString(elapsed, processed);
            previousSize = e.loaded;
            previousTime = currentTime;
        }
    } else {
        progressBar.max = 1;
        progressBar.value = 0;
    }
}

function uploadComplete(evt) {
    /* This event is raised when the server send back a response */
    document.getElementById('messages').innerHTML = evt.target.responseText;
}

function uploadFailed(evt) {
    alert("There was an error attempting to upload the file.");
}

function uploadCanceled(evt) {
    alert("The upload has been canceled by the user or the browser dropped the connection.");
}


function toSpeedString(time,size) {
    var speed = size * 1000 / time;
    if (speed < 1024) {
        return speed.toFixed(0) + " B/s";
    }
    speed = speed / 1024;
    if (speed < 1024) {
        return speed.toFixed(1) + " KB/s";
    }
    speed = speed / 1024;
    return speed.toFixed(2) + " MB/s";
}
