package com.janoz.vertx.dumpster;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;

import java.io.File;

/**
 * Created by gijs on 10/05/16.
 */
public class DumpsterVerticle extends AbstractVerticle{

    private static final Logger LOG = LoggerFactory.getLogger(DumpsterVerticle.class);

    private File uploadDir;
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        uploadDir = new File(config().getString("uploadLocation","upload"));


        if (!uploadDir.exists() || !uploadDir.isDirectory()) {
            LOG.error(String.format("Upload directory '%s' doesn't exist",uploadDir.toString()));
            startFuture.fail("Upload directory doesn't exist");
            return;
        }
        Router router = Router.router(vertx);

        router.put().handler(this::handleUpload);


        router.route("/*").handler(StaticHandler.create()
                .setCachingEnabled(false)
                .setDirectoryListing(false)
        );

        vertx
                .createHttpServer()
                .requestHandler(requestHandler(router))
                .listen(
                        // Retrieve the port from the configuration,
                        // default to 8080.
                        config().getInteger("http.port", config().getInteger("httpd.port",8080)),
                        result -> {
                            if (result.succeeded()) {
                                startFuture.complete();
                            } else {
                                startFuture.fail(result.cause());
                            }
                        }
                );
    }


    private Handler<HttpServerRequest> requestHandler(Router router) {
        return (request) -> {
            LOG.trace(request.absoluteURI());
            router.accept(request);
        };
    }

    private void handleUpload(RoutingContext context) {
        HttpServerRequest req = context.request();
        req.setExpectMultipart(true);
        req.uploadHandler(upload -> {
            String originalFilename = upload.filename();
            String sanatisedFilename = sanatiseFilename(originalFilename);
            File temp = null;
            try {
                temp = File.createTempFile(sanatisedFilename+".",".part",uploadDir);
            } catch (Exception e) {
                LOG.error(e);
                req.response()
                        .setChunked(true)
                        .setStatusCode(500)
                        .end("Upload failed");
                return;
            }
            final File targetFile = temp;



            upload.exceptionHandler(cause -> {
                LOG.error("Error while uploading.", cause);
                req.response()
                        .setChunked(true)
                        .setStatusCode(500)
                        .end("Upload failed");
            });

            upload.endHandler(v -> {
                req.response()
                        .setChunked(true)
                        .end("Successfully uploaded " + upload.filename());
                LOG.info("Done.");
                targetFile.renameTo(new File(targetFile.getParentFile(),targetFile.getName().substring(0,targetFile.getName().lastIndexOf("."))));
            });
            upload.streamToFileSystem(targetFile.getAbsolutePath());
            LOG.info("Uploading " + targetFile);
        });
    }

    private static final String REGEXP = "[^-a-zA-Z0-9._()\\[\\] ]";
    static String sanatiseFilename(String original) {
        return original.replaceAll(REGEXP,"");
    }
}
